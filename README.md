# Lab Hook

Run `scons` to build all STLs or open the `*.scad`-file, select a parameter set in the Customizer, press F6 and then F7 to generate an individual STL file.

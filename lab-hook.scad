// clang-format off
shape_file ="filament-holder-for-big-rail.svg";  // ["filament-holder-for-big-rail.svg","filament-holder-for-small-rail.svg","filament-holder-for-thin-wall.svg"]
// clang-format on

thickness = 10;  // [1:0.1:100]

linear_extrude(thickness) import(shape_file);
